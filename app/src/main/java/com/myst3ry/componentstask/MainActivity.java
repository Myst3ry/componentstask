package com.myst3ry.componentstask;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public final class MainActivity extends AppCompatActivity {

    private Button mainButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        initUI();
    }

    private void initUI() {
        setContentView(R.layout.activity_main);
        initButton();
    }

    private void initButton() {
        mainButton = findViewById(R.id.btn_main);
        mainButton.setOnClickListener(new MainButtonClickListener());
    }

    private static final class MainButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            view.getContext().startActivity(SecondActivity.newIntent(view.getContext()));
        }
    }
}
