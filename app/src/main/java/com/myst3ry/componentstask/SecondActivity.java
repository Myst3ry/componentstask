package com.myst3ry.componentstask;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public final class SecondActivity extends AppCompatActivity {

    private static Drawable drawable;

    public static Intent newIntent(final Context context) {
        return new Intent(context, SecondActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        if (savedInstanceState == null) {
            drawable = getResources().getDrawable(R.drawable.img_deadlock, null);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isFinishing()) {
            drawable = null;
        }
    }
}
