package com.myst3ry.componentstask;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

public final class ComponentsApp extends Application implements Application.ActivityLifecycleCallbacks {

    private static final String TAG = "ComponentsApp";

    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        Log.w(TAG, "Activity " + activity.getLocalClassName() + " created");
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Log.w(TAG, "Activity " + activity.getLocalClassName() + " started");
    }

    @Override
    public void onActivityResumed(Activity activity) {
        Log.w(TAG, "Activity " + activity.getLocalClassName() + " resumed");
    }

    @Override
    public void onActivityPaused(Activity activity) {
        Log.w(TAG, "Activity " + activity.getLocalClassName() + " paused");
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Log.w(TAG, "Activity " + activity.getLocalClassName() + " stopped");
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        Log.w(TAG, "Activity " + activity.getLocalClassName() + " saved instance state");
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.w(TAG, "Activity " + activity.getLocalClassName() + " destroyed");
    }
}
